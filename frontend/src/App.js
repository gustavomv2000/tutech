import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css';

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import LandPage from './components/LandPage/LandPage'
import ProductsPage from './components/ProductsPage/ProductsPage';

function App() {
  return (
    // <section className="main-section">
    <Router>
        <Header />
          <Switch>
            <Route exact path="/">
              <LandPage />
            </Route>
            <Route exact path="/products">
              <ProductsPage />
            </Route>
          </Switch>
      <Footer />
    </Router>
    // </section>
  );
}

export default App;
