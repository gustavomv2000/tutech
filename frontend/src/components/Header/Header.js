import React from 'react';
import { Link } from 'react-router-dom';

import './Header.css';

export default function Header() {
    return(
        <section>
            <header className="header">
                <container className="header-container">
                    <ul className="nav-bar">
                        <li className="logo">
                            <Link to="/" style={{ textDecoration: 'none'}}>
                                <h2>TUtech</h2>
                            </Link>
                        </li>
                        <li className="nav-items">
                            <Link to="/products" style={{ textDecoration: 'none'}}>
                                <p>Produtos</p>
                            </Link>
                        </li>
                        <li className="nav-items">
                            <Link to="/" style={{ textDecoration: 'none'}}>
                                <p>Sobre nós</p>
                            </Link>
                        </li>
                    </ul>
                </container>
            </header>
        </section>
    )
}