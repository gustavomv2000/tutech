import React from 'react';

import "./LandPage.css"
import Velocimetro from "../../assets/img/velocimetro.png";

export default function LandPage() {
    return(
        <section className="landpage-section">
            <container className="landpage-container">
                <img className="velocimetro" alt="" src={Velocimetro} />
                <div className="land-text">
                    <div className="text-wrapper">
                        <h2 className="land-title">Melhore o seu projeto aqui</h2>
                        <p className="land-phrase">Encontre os equipamentos perfeitos</p>
                    </div>
                </div>
            </container>
        </section>
    )
}