import React from 'react';
import {Link} from 'react-router-dom';

import './Footer.css';
import WhatsApp from "../../assets/img/whatsapp.png";
import Facebook from "../../assets/img/facebook.png";
import Instagram from "../../assets/img/instagram.png";
import LinkedIn from "../../assets/img/linkedin.png";

export default function Footer() {
    return(
        <footer>
            <div className="footer-container">
                <ul className="footer-bar">
                    <li className="sub-logo">
                        <h2>TUtech</h2>
                    </li>
                    <li className="social-media">
                        <Link to="/" style={{ textDecoration: 'none'}}>
                            <img className="social-media-logo" alt="(11) 96647-6577" src={WhatsApp} />
                        </Link>
                    </li>
                    <li className="social-media">
                        <Link to="/" style={{ textDecoration: 'none'}}>
                            <img className="social-media-logo" alt="gmorales_00" src={Instagram} />
                        </Link>
                    </li>
                    <li className="social-media">
                        <Link to="/" style={{ textDecoration: 'none'}}>
                            <img className="social-media-logo" alt="Gustavo Morales Vieira" src={Facebook} />
                        </Link>
                    </li>
                    <li className="social-media">
                        <Link to="/" style={{ textDecoration: 'none'}}>
                            <img className="social-media-logo" alt="Gustavo Morales Vieira" src={LinkedIn} />
                        </Link>
                    </li>
                </ul>
            </div>
        </footer>
    )
}